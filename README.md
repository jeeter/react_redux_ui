# react_redux_ui

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### start:dev 

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### start

Runs the production start script on a production build. This script is executed as the `CMD` in the Docker Image.

It serves the app over PORT 5000.

### build

Creates the production build in directory `./build`.

### test

Runs the `react` unit tests.

### docker-up

Launches the application in a dockerized container still accessible at http://localhost:3000 in the interactive watch mode.
